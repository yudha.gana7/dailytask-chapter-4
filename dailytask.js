// 1. buat array of object students ada 5 data cukup
const student = [{
        nama: 'Dodit Ceper',
        asal: 'Jawa Tengah',
        umur: 22,
        status: 'single',
    },
    {
        nama: 'Bobi Separo',
        asal: 'Jawa Barat',
        umur: 20,
        status: 'single',
    },
    {
        nama: 'Dodot Salto',
        asal: 'Jawa Tengah',
        age: 27,
        status: 'menikah',
    },
    {
        nama: 'Andre Nusantara',
        asal: 'Jawa Timur',
        umur: 20,
        status: 'single',
    },
    {
        nama: 'Bambang Keplere',
        asal: 'Jawa Barat',
        umur: 25,
        status: 'single',
    },
];

// 2. buat 3 function, 1 function nentuin provinsi kalian ada di jawa barat atau tidak, umur kalian diatas 22 atau tidak, status kalian single atau gak
function cekAlamat(asal) {
    return (asal == 'Jawa Barat' ? 'tinggal di Jawa Barat' : 'tidak tinggal di Jawa Barat');
}
// console.log(cekAlamat(true));

function cekUmur(umur) {
    return (umur < 22 ? 'di bawah 22 tahun' : 'Umur lebih dari 22 tahun');
}
// console.log(cekUmur(30));

function cekStatus(status) {
    return (status ? 'single' : 'menikah');
}
// console.log(cekStatus(true));


// 3. callback funtion untuk print hasil proses 3 funtion diatas.
function printDataSiswa(callback1, callback2, callback3) {
    for (const element of student) {
        // console.log(index);
        let checkAlamat = callback1(element.asal);
        // console.log(index.asal);
        let checkUmur = callback2(element.umur);
        // console.log(index.umur);
        let checkStatus = callback3(element.status);
        // console.log(index.status);
        if (
            checkAlamat == 'tinggal di Jawa Barat' &&
            checkUmur == 'di bawah 22 tahun' &&
            checkStatus == 'single'
        ) {
            // nama saya imam, saya tinggal di jawa barat, umur saya dibawah 22 tahun. dan status saya single loh.
            console.log(`Nama Saya adalah ${element.nama}, Saya ${checkAlamat}, Umur Saya ${checkUmur}, dan status saya ${checkStatus} lohh`);
        }
    }
}

printDataSiswa(cekAlamat, cekUmur, cekStatus);