// const helloWorld = () => {
//     console.log('Hello World');
//     let total = 100 + 1000
//     console.log(total)
// }

// const hi = (callbackk) => {
//     console.log('Hi')
//     callbackk();
// }

// hi(helloWorld)

// hi(function test() {
//     console.log('ini function baru');
// })

// const student = (callback) => {
//     callback("Kami FSW 13", 25)
// }

// student((perkenalan, jumlah_murid) => {
//     console.log(`Perkenalkan ${perkenalan} dengan ${jumlah_murid} murid !`)
// })

const strArray = ['JavaScript', 'Java', 'C'];

function forEach(array, callback) {
    const newArray = [];
    for (let i = 0; i < array.length; i++) {
        // console.log(array[i])
        newArray.push(callback(array[i]));
    }
    return newArray;
}

const lenArray = forEach(strArray, (item) => {
    console.log(item)
    return item.length;
});
console.log(lenArray);
// Output: [ 10, 4, 1 ]